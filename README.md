<center>

Trabalho da disciplina de Web1
Sistema completo: Agenda de eventos online / Calendário

Funções que precisa ter na aplicação:

<hr>

CALENDARIO (BRANCH calendario)

[- Exibição de calendário na página principal]
[- Exibição e interação de eventos e contatos no calendário]

</hr>

<hr>
CRUD AUTENTIFICAÇÃO DE USUARIO (BRANCH autenticacao-usuarios)

[ - Cadastro de usuários. ]
[ - Login de usuários. ]
[ - Validação de credenciais. ]
[ - Armazenamento de senhas seguro.]
[ - Logout. ]

</hr>

<hr>
 CRUD EVENTOS (BRANCH eventos)

[- Função para adicionar novos eventos ao calendário.]

[- Função para editar eventos existentes no calendário.]

[- Função para excluir eventos do calendário.]

[- Função para listar todos os eventos em um calendário.]

[- Função para adicionar descrições detalhadas aos eventos.]

[- Função para especificar a hora de início e término dos eventos.]

[- Função para lidar com eventos que se estendem por mais de um dia no calendário.]

<!-- <!-- [- Função para permitir que os usuários convidem outros usuários a eventos e para permitir que os usuários respondam a esses convites.] ///opcional? -->

[- Função para permitir que os usuários compartilhem eventos com outras pessoas, por exemplo, por meio de convites por e-mail ou mensagens.]

</hr>

<hr>
 CRUD CONTATOS (BRANCH contatos)

[- Função de criar contatos. ]

[- Função de ler os contatos. ]

[- Função de atualizar contatos. ]

[- Função de excluir contatos. ]

</hr>

<hr>
 GERAL

[- Interface de usuário: Implementação de interface de usuário para permitir que os usuários interajam com a aplicação.]

[- Responsividade: Implementação de design responsivo para garantir que o site seja exibido corretamente em diferentes tamanhos de tela.]

[- Armazenamento de dados: Implementação de sistema de armazenamento de dados para armazenar informações sobre eventos.]

[- Segurança: Implementação de medidas de segurança para proteger os dados do usuário.]

</hr>
